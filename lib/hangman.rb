class Hangman
  attr_accessor :guesser, :referee, :board

  def initialize(options={})
    # default = { :guesser => HumanPlayer.new("empty"),
    #             :referee => ComputerPlayer.new("empty"),
    #             :board => [] }
    # options = default.merge(options)

    @guesser = options[:guesser]
    @referee = options[:referee]
    @board = []
  end #initialize

  def setup
    secret_length = referee.pick_secret_word
    guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end #setup

  def take_turn
    referee.check_guess(guesser.guess)
    update_board
    guesser.handle_response
  end #take_turn

  def update_board
    #This should take 2 arguements from where it was called in take_turn
    #but does not pass spec test if passed in 2 arguments
  end
end #class Hangman

class HumanPlayer
end #class HumanPlayer

class ComputerPlayer
attr_accessor :dictionary, :secret_word, :secret_length, :board

  def initialize(dictionary)
    @dictionary = dictionary
    @board = []
  end #initialize

  def pick_secret_word
    @secret_word = @dictionary[rand(@dictionary.length)]
    @secret_word.length
  end #pick_secret_word

  def check_guess(letter)
    index = []
    @secret_word.each_char.with_index {|l, i| index << i if l == letter}
    index
  end #check_guess

  def register_secret_length(len)
    @secret_length = len
    @board = Array.new(len, nil) if @board.empty?
    @dictionary = dictionary.select {|w| w.length == len}
  end #register_secret_length

  def guess(board)
    let_cnt_hsh = Hash.new(0)
    candidate_words.join.each_char {|l| let_cnt_hsh[l] += 1}

    let_cnt_hsh = let_cnt_hsh.select {|k, v| !board.include?(k)}

    max = let_cnt_hsh.values.max
    let_cnt_hsh = let_cnt_hsh.select {|k, v| v == max}
    let_cnt_hsh.keys[0]
  end #guess

  def handle_response(l, idx)
    if idx.length > 0
      @dictionary = dictionary.select {|w| match_letter?(w, l, idx) }
    else
      @dictionary = dictionary.select {|w| !w.include?(l) }
    end
    idx.each {|i| @board[i] = l}
    puts "Board"
    p @board
  end #handle_response

  def match_letter?(w, l, pos)
    pos.each {|i| return false unless w[i] == l}
    return false if pos.length != w.count(l)
    true
  end #match_letter?

  def candidate_words
    dictionary
  end #candidate_words
end #class ComputerPlayer
